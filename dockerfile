FROM node:21.2.0-alpine

RUN npm install -g http-server

## RUN npm install -g npm@10.3.0

# hacer la carpeta 'app' el directorio de trabajo actual
WORKDIR /app

# copiar 'package.json' y 'package-lock.json' (si están disponibles)
COPY package.json package-lock.json ./

# instalar dependencias del proyecto
RUN npm install

# copiar los archivos y carpetas del proyecto al directorio de trabajo actual (es decir, la carpeta 'app')
COPY . .

# construir aplicación para producción minificada
#RUN npm run build

EXPOSE 30890

CMD ["npm", "run", "serve", "--", "--port", "30890", "--mode", "prod", "--host", "0.0.0.0", "--public", "kaumon.cl", "--use-https", "true"]